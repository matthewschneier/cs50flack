from flask_wtf import FlaskForm
from wtforms import SubmitField, TextField, validators

class UsernameForm(FlaskForm):
    username = TextField("Username: ", validators=[
        validators.required(), validators.Length(min=3, max=30)
    ])
    submit = SubmitField()

class ChannelForm(FlaskForm):
    channel = TextField("Channel: ", validators=[
        validators.required(), validators.Length(min=3, max=99)
    ])
    submit = SubmitField()

from flask import Flask, flash, render_template, redirect, request, session
from flask_socketio import SocketIO, send, emit, join_room, leave_room
from flask_wtf.csrf import CSRFProtect
from forms import ChannelForm, UsernameForm
from functools import wraps

app = Flask(__name__)
app.config["SECRET_KEY"] = "asfl;jasdklfj;lkjlk;23j4"
csrf = CSRFProtect(app)
socketio = SocketIO(app)

channels = []

users = []

messagesByChannel = {}


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not session.get("username"):
            return redirect("/login")
        return f(*args, **kwargs)
    return decorated_function


@app.route("/", methods=["GET", "POST"])
@login_required
def index():
    form = ChannelForm()
    return render_template("index.html", form=form, channels=channels)


@app.route("/login", methods=["GET","POST"])
def login():
    "Save username to session if it's unique."

    # Forget existing username.
    session.clear()

    form = UsernameForm()
    if form.validate_on_submit():
        username = form.username.data
        if username in users:
            flash("Username already exists.")
            return redirect("/login")
        
        users.append(username)
        session["username"] = username

        # Remember the user session on a cookie if the browser is closed.
        session.permanent = True

        return redirect("/")
    return render_template("login.html", form=form)


@app.route("/logout")
@login_required
def logout():
    "Logout user and delete their cookie"

    # Remove from list.
    users.remove(session["username"])
    # Remove username from session.
    session.clear()

    return redirect("/login")


@app.route("/create", methods=["GET","POST"])
def create():
    "Create new channel if it doesn't exist."

    form = ChannelForm()
    if form.validate_on_submit():
        newChannel = form.channel.data

        if newChannel in channels:
            flash("That channel already exists.")
            return redirect("/create")
                
        # Add channel to global list of channels.
        channels.append(newChannel)

        # Add channel to global dict of channels with messages.
        messagesByChannel[newChannel] = []

        return redirect(f"/channels/{newChannel}")
    return render_template("index.html", form=form)


@app.route("/channels/<channel>", methods=["GET","POST"])
@login_required
def enter_channel(channel):
    "Show channel."

    # Updates user current channel.
    session["current_channel"] = channel

    if request.method == "POST":
        return redirect("/")
    else:
        return render_template(
            "channel.html", channels=channels,
            messages=messagesByChannel[channel]
        )


@login_required
@socketio.on("joined", namespace="/")
def joined():
    "Announce user has entered channel."
    # Save current channel to join room.
    room = session.get("current_channel")

    join_room(room)
    emit("status", {
            "userJoined": session["username"],
            "channel": room,
            "msg": f"{session['username']} has entered the channel"
        }, room=room
    )


@login_required
@socketio.on("left", namespace="/")
def left():
    "Announce user has left channel."
    room = session.get("current_channel")
    leave_room(room)
    emit("status", {
            "msg": f"{session['username']} has left the channel"
        }, room=room
    )


@login_required
@socketio.on("send message")
def send_msg(msg, timestamp):
    "Announce and broadcast message with timestamp."

    # Broadcast only to users on the same channel.
    room = session.get("current_channel")

    messagesByChannel[room].append(
        [timestamp, session.get("username"), msg]
    )
    emit("announceMessage", {
            "user": session["username"],
            "timestamp": timestamp,
            "msg": msg
        }, room=room
    )

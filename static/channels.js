$(document).ready(() => {
    const socket = io.connect(
        `${location.protocol}//${document.domain}:${location.port}`
    );

    socket.on("connect", () => {
        socket.emit("joined");
        $("#newChannel").on("click", () => {
            localStorage.removeItem("last_channel");
        });

        $("#leave").on("click", () => {
            socket.emit("left");
            localStorage.removeItem("last_channel");
            window.location.replace("/channel");
        });

        $("#logout").on("click", () => {
            localStorage.removeItem("last_channel");
        })

        $("#comment").on("keydown", (event) => {
            if (event.key == "Enter") {
                $("#send-button").click();
            }
        });

        $("#send-button").on("click", () => {
            let timestamp = new Date;
            timestamp = timestamp.toLocaleTimeString();
            const msg = $("#comment").val();
            socket.emit("sendMessage", msg, timestamp);
            $("#comment").val("");
        });
    });

    socket.on("status", (data) => {
        const row = `<${data.msg}>`;
        document.getElementById("chat").value += `${row}\n`;
        localStorage.setItem("last_channel", data.channel);
    })

    socket.on("announceMessage", (data) => {
        const row = `<${data.timestamp}> - [${data.user}]: ${data.msg}`;
        document.getElementById("chat").value += `${row}\n`;
    })
});